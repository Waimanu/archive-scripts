#!/usr/bin/env bash

vFilmDir="/home/yosijo/Waimanu/Videos/Filme"

for vFile in *.mkv; do
  echo "File Name: '${vFile}'"

  vPreMovieName="$(echo ${vFile} | grep -Po '^[^[]+')"
  vMovieName="${vPreMovieName% }"
  echo "Movie name: '${vMovieName}'"

  echo "${vMovieName}" | grep -Pq '_' && continue
  echo "${vMovieName}" | grep -Pq '(Teil|OVA) [0-9]+$' && continue

  vMovieInfo="$(echo ${vFile} | grep -Po '\[[^[]+\]')"
  echo "Movie info: '${vMovieInfo}'"

  vMovieYear="$(echo ${vMovieInfo} | grep -Po '^\[[^,]+,\K[0-9]+')"
  echo "Movie info: '${vMovieYear}'"
  echo "${vMovieYear}" | grep -Pqv '[0-9]{4}' && continue

  mkdir -p "${vFilmDir}/${vMovieYear}/${vMovieName}"
  mv -v "${vFile}" "${vFilmDir}/${vMovieYear}/${vMovieName}/"

  echo "Alles gut."
  break
done

#!/usr/bin/env bash
set -e

# Variables
regex_file_suffix='mkv'
regex_px='(720|1080)p'
regex_video_codec='x26[45]'
regex_audio_codec='(Opus|DTS)'
regex_lang_codes='DE(-(|EN|FR|HI|ES|NO|IT|frCA|JP|DA))+'
regex_year='[0-9]{4}'
regex_gerne='(|Abenteuer|Action|Animation|Drama|Dokumentarfilm|Familie|Fantasy|Historie|Horror|Komödie|Krimi|Liebesfilm|Mystery|Science Fiction|Thriller|TV-Film|Western)'
regex_suffix='(| ((|Directors|Extended)(| )(|Un)[cC]ut|Remastered))'
regex_porn_studios='((Babes|Blacked|TUSHY|Vixen)\.com)'

function movie_sort() {
	movie_file_name="$1"
	echo "Movie file: ${movie_file_name}"
	movie_meta="$(echo "${movie_file_name}" | grep -Po "\[[a-zA-ZäÄöÖüÜß0-9-, ]+\]")"
	movie_name="$(echo "${movie_file_name}" | grep -Po "^[^[]+")"
	movie_year="$(echo "${movie_meta}" | grep -Po "^\[${regex_gerne},\K${regex_year}")"

	echo "Movie Name:   ${movie_name}"
	echo "Movie Year:   ${movie_year}"
	echo "Movie Meta:   ${movie_meta}"

	mkdir -p "../Videos/Filme/${movie_year}/${movie_name% }"
  mv --no-clobber --verbose "./${movie_file_name}" "../Videos/Filme/${movie_year}/${movie_name% }/"
}

function porn_sort() {
	porn_file_name="$1"
	echo "Pine file: ${porn_file_name}"

	porn_studio_name="$(echo "${porn_file_name}" | grep -Po "^${regex_porn_studios}")"

	echo "Porn Studio: ${porn_studio_name}"

	mkdir -p "../Porn/Videos/${porn_studio_name}"
	mv --no-clobber --verbose "./${porn_file_name}" "../Porn/Videos/${porn_studio_name}/"
}

for file_name in *; do
	#Filme
	if [[ $file_name =~ ^.*\ \[${regex_gerne},${regex_year},${regex_lang_codes},${regex_audio_codec},${regex_video_codec},${regex_px}\]${regex_suffix}\.${regex_file_suffix}$ ]]; then movie_sort "${file_name}"; continue; fi

	# Porn
	if [[ $file_name =~ ^(${regex_porn_studios}.*)$ ]]; then porn_sort "${file_name}"; continue; fi
done

#!/usr/bin/env bash

function __get_meta_data_from_filename() {
  _input "${1}"
  _meta_data="${_input##*[}"
  _meta_data="${_meta_data%%]*}"
  echo "${_meta_data}"
}

function __sort_movies() {
	_movie_file_name="${1}"
	[ -v _verbose ] && echo "Movie file: ${_movie_file_name}"
	movie_meta="$( __get_meta_data_from_filename "${movie_file_name}" )"
	movie_name="$(echo "${movie_file_name}" | grep -Po "^[^[]+")"
	movie_year="$(echo "${movie_meta}" | grep -Po "^\[${regex_gerne},\K${regex_year}")"

	echo "Movie Name:   ${movie_name}"
	echo "Movie Year:   ${movie_year}"
	echo "Movie Meta:   ${movie_meta}"

	mkdir -p "../Videos/Filme/${movie_year}/${movie_name% }"
  mv --no-clobber --verbose "./${movie_file_name}" "../Videos/Filme/${movie_year}/${movie_name% }/"
}


#!/usr/sbin/env bash

find . -maxdepth 1 -type f -regextype posix-extended -iregex '.*\.(mp3|wav|wma|m4a|ogg|mpg)$' -print0 |
  while IFS= read -r -d '' filepath; do
    echo "Filepath:                       ${filepath}"

    filename="${filepath#./}"
    echo "Filename:                       ${filename}"

    filename_fist_char="${filename:0:1}"
    echo "Filename fist char:             ${filename_fist_char}"

    filename_fist_char_uppercase="${filename_fist_char^}"
    echo "Filename first char uppercase:  ${filename_fist_char_uppercase}"

    [ -d "./${filename_fist_char_uppercase}" ] || mkdir -v "./${filename_fist_char_uppercase}"

    mv -v "${filepath}" "./${filename_fist_char_uppercase}/"
  done
